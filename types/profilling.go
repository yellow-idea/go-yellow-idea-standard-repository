package types

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
)

type ProFilling struct {
	ID              primitive.ObjectID `json:"id" bson:"_id"`
	CampaignVersion int                `json:"campaign_version" bson:"campaign_version"`
	Name            string             `json:"name" bson:"name"`
	CreatedAt       time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time          `json:"updated_at" bson:"updated_at"`
}
