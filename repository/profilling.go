package repository

import (
	"bitbucket.org/yellow-idea-standard-repository/interfaces"
	"bitbucket.org/yellow-idea-standard-repository/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

func NewProFillingRepository() interfaces.ProFillingRepository {
	return &proFillingRepository{
	}
}

type proFillingRepository struct {
}

func (x *proFillingRepository) List(db *mongo.Database, filter interface{}) {

}

func (x *proFillingRepository) Show(db *mongo.Database, mid string) {

}

func (x *proFillingRepository) Create(db *mongo.Database, payload *types.ProFilling) {
	// Execute
	db.Collection("")
	payload.ID = primitive.NewObjectID()
}

func (x *proFillingRepository) Update(db *mongo.Database, mid string, data *types.ProFilling) {

}

func (x *proFillingRepository) ToggleActive(db *mongo.Database, mid string) {

}
