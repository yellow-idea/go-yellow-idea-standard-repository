package interfaces

import (
	"bitbucket.org/yellow-idea-standard-repository/types"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserRepository interface {
	List(filter interface{})
	Show(mid string)
	Create(data interface{})
	Update(mid string, data interface{})
	ToggleActive(mid string)
}

type LineBCSettingRepository interface {
	List(filter interface{})
	Show(mid string)
	Create(data interface{})
	Update(mid string, data interface{})
	ToggleActive(mid string)
}

type LineProfileRepository interface {
	List(filter interface{})
	Show(mid string)
	Create(data interface{})
	Update(mid string, data interface{})
	ToggleActive(mid string)
}

type LineCampaignRepository interface {
}

type LineAutoReplyDefaultRepository interface {
}

type LineAutoReplyKeywordRepository interface {
}

type LineRichMenuRepository interface {
}

type SegmentRepository interface {
	List(filter interface{})
	Show(mid string)
	Create(data interface{})
	Update(mid string, data interface{})
	ToggleActive(mid string)
}

type LineTrackingRepository interface {
	List(filter interface{})
	Show(mid string)
	Create(data interface{})
	Update(mid string, data interface{})
	ToggleActive(mid string)
}

type ProFillingRepository interface {
	List(db *mongo.Database, filter interface{})
	Show(db *mongo.Database, mid string)
	Create(db *mongo.Database, data *types.ProFilling)
	Update(db *mongo.Database, mid string, data *types.ProFilling)
	ToggleActive(db *mongo.Database, mid string)
}
